using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class loginScreen : Form
    {
        public loginScreen()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void loginScreen_Load(object sender, EventArgs e)
        {

        }
    }
}
