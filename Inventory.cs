using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    class Inventory
    {
        List<InventoryItem> inventoryList = new List<InventoryItem>();
        private char delimiter = ',';
        private string header1;
        private string header2;

        public List<InventoryItem> ReadInventoryFile()
        {
            try
            {
                StreamReader streamReader = new StreamReader(File.OpenRead("inventory.csv"));

                header1 = streamReader.ReadLine();
                header2 = streamReader.ReadLine();

                while (!streamReader.EndOfStream)
                {
                    string inventoryLine = streamReader.ReadLine();
                    string[] inventoryStrings = inventoryLine.Split(delimiter);

                    InventoryItem inventoryItem = new InventoryItem();
                    inventoryItem.Type = inventoryStrings[0];
                    inventoryItem.Name = inventoryStrings[1];
                    inventoryItem.Brand = inventoryStrings[2];
                    inventoryItem.Quantity = int.Parse(inventoryStrings[3]);
                    inventoryItem.Price = decimal.Parse(inventoryStrings[4]);

                    inventoryList.Add(inventoryItem);
                }

                streamReader.Dispose();
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            return inventoryList;
        }

        public void SaveInventoryFile()
        {
            try
            {
                StreamWriter streamWriter = new StreamWriter(File.OpenWrite("inventory.csv"));

                streamWriter.WriteLine(header1);
                streamWriter.WriteLine(header2);

                foreach (InventoryItem inventoryItem in inventoryList)
                {
                    streamWriter.WriteLine(inventoryItem.Type + "," + inventoryItem.Name + "," + inventoryItem.Brand + "," + inventoryItem.Quantity + "," + inventoryItem.Price);
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }
    }
}
