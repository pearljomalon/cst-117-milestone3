using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class addItemScreen : Form
    {
        InventoryManager inventoryManager = new InventoryManager();

        public addItemScreen()
        {
            InitializeComponent();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            inventoryManager.AddItem(CreateInventoryItem());
        }

        private InventoryItem CreateInventoryItem()
        {
            InventoryItem inventoryItem = new InventoryItem();

            //if (MessageBox.Show("Confirm save new item?", "New Item", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                inventoryItem.Type = typeTextBox.Text.ToUpper();
                inventoryItem.Name = nameTextBox.Text.ToUpper();
                inventoryItem.Brand = brandTextBox.Text.ToUpper();
                inventoryItem.Quantity = (int)quantityUpDown.Value;
                inventoryItem.Price = priceUpDown.Value;

                typeTextBox.Text = "";
                nameTextBox.Text = "";
                brandTextBox.Text = "";
                quantityUpDown.Value = 0;
                priceUpDown.Value = 0;

                MessageBox.Show("Item was successfully added");

                return inventoryItem;
            }

            return default(InventoryItem);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            inventoryScreen inventoryScreen = new inventoryScreen();

            this.Hide();
            inventoryScreen.Show();
        }
    }
}
