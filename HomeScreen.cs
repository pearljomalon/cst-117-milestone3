using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class homeScreen : Form
    {   
        public homeScreen()
        {
            InitializeComponent();
        }

        private void viewInventoryButton_Click(object sender, EventArgs e)
        {
            inventoryScreen inventoryScreen = new inventoryScreen();

            this.Hide();
            inventoryScreen.Show();
        }

        private void searchInventoryButton_Click(object sender, EventArgs e)
        {
            searchScreen searchScreen = new searchScreen();

            this.Hide();
            searchScreen.Show();
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            addItemScreen addItemScreen = new addItemScreen();

            this.Hide();
            addItemScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
