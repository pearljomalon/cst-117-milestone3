using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class individualScreen : Form
    {
        public individualScreen()
        {
            InitializeComponent();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void individualScreen_Load(object sender, EventArgs e)
        {
            typeTextBox.Text = Program.thisInventoryItem.Type;
            nameTextBox.Text = Program.thisInventoryItem.Name;
            brandTextBox.Text = Program.thisInventoryItem.Brand;
            quantityUpDown.Value = Program.thisInventoryItem.Quantity;
            priceUpDown.Value = Program.thisInventoryItem.Price;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            typeTextBox.ReadOnly = true;
            nameTextBox.ReadOnly = true;
            brandTextBox.ReadOnly = true;
            quantityUpDown.ReadOnly = true;
            priceUpDown.ReadOnly = true;

            typeTextBox.Text = Program.thisInventoryItem.Type;
            nameTextBox.Text = Program.thisInventoryItem.Name;
            brandTextBox.Text = Program.thisInventoryItem.Brand;
            quantityUpDown.Value = Program.thisInventoryItem.Quantity;
            priceUpDown.Value = Program.thisInventoryItem.Price;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            confirmChanges();
        }

        private void  confirmChanges()
        {
            if (MessageBox.Show("Confirm changes?", "Edit item", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Program.savedChanges = true;

                InventoryItem inventoryItem = new InventoryItem();

                inventoryItem.Type = typeTextBox.Text;
                inventoryItem.Name = nameTextBox.Text;
                inventoryItem.Brand = brandTextBox.Text;
                inventoryItem.Quantity = (int)quantityUpDown.Value;
                inventoryItem.Price = priceUpDown.Value;

                Program.thisInventoryItem = inventoryItem;
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            typeTextBox.ReadOnly = false;
            nameTextBox.ReadOnly = false;
            brandTextBox.ReadOnly = false;
            quantityUpDown.ReadOnly = false;
            priceUpDown.ReadOnly = false;
        }
    }
}
