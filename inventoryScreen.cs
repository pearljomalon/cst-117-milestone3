using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class inventoryScreen : Form
    {
        List<InventoryItem> inventoryList = Program.inventoryList;
        InventoryManager inventoryManager = new InventoryManager();

        private int selectedRow;

        public inventoryScreen()
        {
            InitlializeComponent();

            loadInventoryDataGrid();

            if (Program.savedChanges)
            {
                updateItemInGrid();
                updateInventoryList();
                loadInventoryDataGrid();
            }

            inventoryManager.SaveInventoryFile();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            individualScreen individualScreen = new individualScreen();

            Program.thisInventoryItem = viewInventoryItem();

            this.Hide();
            individualScreen.Show();
        }
    }
}
