using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class searchScreen : Form
    {
        List<InventoryItem> searchList = new List<InventoryItem>();
        private static string searchString;

        public searchScreen()
        {
            InitializeComponent();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            individualScreen individualScreen = new individualScreen();

            this.Hide();
            individualScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void typeRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void nameRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void brandRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SearchScreen_Load(object sender, EventArgs e)
        {

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            searchList.Clear();
            Search();
        }

        private void Search()
        {
            searchString = searchTextBox.Text.ToUpper();

            searchDataGrid.Rows.Clear();
            searchDataGrid.Refresh();

            int row = 0;

            searchList = Program.inventoryList.FindAll(Search);

            if (searchList.Count != 0)
            {
                foreach (InventoryItem item in searchList)
                {
                    searchDataGrid.Rows.Insert(row);
                    searchDataGrid[0, row].Value = item.Type;
                    searchDataGrid[1, row].Value = item.Name;
                    searchDataGrid[2, row].Value = item.Brand;
                    searchDataGrid[3, row].Value = item.Quantity;
                    searchDataGrid[4, row].Value = item.Price;

                    row++;
                }
            }

            else MessageBox.Show("NO RESULTS FOUND");
        }

        private bool Search(InventoryItem item)
        {
            if (typeRadioButton.Checked)
            {
                if (item.Type.Contains(searchString))
                {
                    return true;
                }

                return false;
            }

            else if (nameRadioButton.Checked)
            {
                if (item.Name.Contains(searchString))
                {
                    return true;
                }

                return false;
            }

            else if (brandRadioButton.Checked)
            {
                if (item.Brand.Contains(searchString))
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}
