using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    static class Program
    {
        public static List<InventoryItem> inventoryList = new List<InventoryItem>();

        public static InventoryItem thisInventoryItem;

        public static bool savedChanges;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Inventory inventory = new Inventory();
            inventoryList = inventory.ReadInventoryFile();

            InventoryManager inventoryManager = new InventoryManager();
            inventoryManager.saveBackup();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new loginScreen());
        }
    }
}
