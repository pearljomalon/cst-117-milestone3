using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    class InventoryManager
    {
        List<InventoryItem> inventoryList = Program.inventoryList;
        private string header1 = "TYPE,NAME,BRAND,QUANTITY,PRICE";
        private string header2 = ",,,,";

        public void DeleteItem(InventoryItem inventoryItem)
        {
            inventoryList.Remove(inventoryItem);
            SaveInventoryFile();
        }

        public void AddItem(InventoryItem inventoryItem)
        {
            inventoryList.Add(inventoryItem);
            SaveInventoryFile();
        }

        public void UpdateItem(InventoryItem inventoryItem)
        {
            Program.savedChanges = true;
            Program.thisInventoryItem = inventoryItem;
            SaveInventoryFile();
        }

        public void SaveInventoryFile()
        {
            try
            {
                FileStream fcreate = File.Open("inventory.csv", FileMode.Create);

                StreamWriter streamWriter = new StreamWriter(fcreate);

                streamWriter.WriteLine(header1);

                foreach(InventoryItem inventoryItem in inventoryList)
                {
                    streamWriter.WriteLine();
                    streamWriter.Write(inventoryItem.Type + "," + inventoryItem.Name + "," + inventoryItem.Brand + "," + inventoryItem.Quantity + "," + inventoryItem.Price);
                }

                streamWriter.Dispose();
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        public void saveBackup()
        {
            try
            {
                FileStream fcreate = File.Open("inventoryBACKUP.csv", FileMode.Create);

                StreamWriter streamWriter = new StreamWriter(fcreate);

                streamWriter.WriteLine(header1);

                foreach (InventoryItem inventoryItem in inventoryList)
                {
                    streamWriter.WriteLine();
                    streamWriter.Write(inventoryItem.Type + "," + inventoryItem.Name + "," + inventoryItem.Brand + "," + inventoryItem.Quantity + "," + inventoryItem.Price);
                }

                streamWriter.Dispose();
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }
    }
}
